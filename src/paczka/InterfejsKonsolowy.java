package paczka;

/**
 * @author Andrzej
 *Klasa reprezentujaca interfejs konsolowy
 */
public class InterfejsKonsolowy extends InterfejsAbs {
	public static int licznikInterfejsowKonsolowych=0;

	InterfejsKonsolowy(String nazwaInterfejsu, String adresIpInterfejsu, String maskaPodsieci) {
		super(adresIpInterfejsu, maskaPodsieci);
		// TODO Auto-generated constructor stub
		rodzajTegoInterfejsu=RodzajInterfejsu.KONSOLOWY;
		nazwaTegoInterfejsu=nazwaInterfejsu.substring(2);
		licznikInterfejsowKonsolowych++;
	}
	
}
