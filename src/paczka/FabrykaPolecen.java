package paczka;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author Andrzej
 *Klada odpowiedzialna za wywolywanie odpowiednich polecen 
 */
public class FabrykaPolecen {
	public Router router=null;
	public FabrykaInterfejsow fabrykaInterfejsow=null;
	public InetAddress adresDoWylapaniaBledu=null;
	public FabrykaPolecen(Router router) {
		this.router=router;
		fabrykaInterfejsow=new FabrykaInterfejsow(this.router);
	}
	/**
	 * @param czescPierwszaPolecenia
	 * @param czescDrugaPolecenia
	 * @return
	 * Funkcja w odpowiedzi na zadana przez uzytkownika komende zwraca odpowiednie polecenie
	 */
	public Polecenie stworzObiektPolecenia(String czescPierwszaPolecenia, String czescDrugaPolecenia) {
		Polecenie polecenieDoZwrocenia=null;
		if((czescPierwszaPolecenia==null) && (czescDrugaPolecenia==null)) {
			polecenieDoZwrocenia=new BrakPolecenia();
			return polecenieDoZwrocenia;
		}
		
		try {
			adresDoWylapaniaBledu=InetAddress.getByName(czescDrugaPolecenia);
		} catch (UnknownHostException e) {
			polecenieDoZwrocenia=new BlednePolecenie();
			return polecenieDoZwrocenia;
		}
		if(this.czyInterfejsJuzIstnieje(czescPierwszaPolecenia)) {
			polecenieDoZwrocenia=new KomunikatOIstniejacymInterfejsie();
		}
		
		else if(czescPierwszaPolecenia.equalsIgnoreCase("help") && (czescDrugaPolecenia==null)) {
			polecenieDoZwrocenia=new PolecenieHelp();
		}
		else if(czescPierwszaPolecenia.equalsIgnoreCase("show") && (czescDrugaPolecenia==null)) {
			polecenieDoZwrocenia=new PolecenieShow(router);
		}
		else if(czescPierwszaPolecenia.substring(0,3).equalsIgnoreCase("ipA") && (czescDrugaPolecenia!=null)) {
			polecenieDoZwrocenia=new PolecenieIP(czescPierwszaPolecenia, czescDrugaPolecenia, this.router);
		}
		else if(czescPierwszaPolecenia.substring(0,3).equalsIgnoreCase("ipB") && (czescDrugaPolecenia!=null)) {
			polecenieDoZwrocenia=new PolecenieIP(czescPierwszaPolecenia, czescDrugaPolecenia, this.router);
		}
		else if(czescPierwszaPolecenia.substring(0,3).equalsIgnoreCase("ipC") && (czescDrugaPolecenia!=null)) {
			polecenieDoZwrocenia=new PolecenieIP(czescPierwszaPolecenia, czescDrugaPolecenia, this.router);
		}
		
		else if(czescPierwszaPolecenia.equalsIgnoreCase("shutdown") && (czescDrugaPolecenia==null)) {
			polecenieDoZwrocenia=new PolecenieWylacz();
		}
		else if(czescPierwszaPolecenia.equalsIgnoreCase("simA") && (czescDrugaPolecenia!=null)) {
			polecenieDoZwrocenia=new PolecenieSimA(router, czescDrugaPolecenia);
		}
		else {
			polecenieDoZwrocenia=new BlednePolecenie();
		}
		
		return polecenieDoZwrocenia;
	}
	
	/**
	 * @param nazwaInterfejsu
	 * @return
	 * Funkcja sprawdzajaca czy interfejs o podanej nazwie juz istnieje
	 */
	public boolean czyInterfejsJuzIstnieje(String nazwaInterfejsu) {
		boolean czyIstnieje=false;
		for (InterfejsAbs interfejs : router.zbiorInterfejsowRoutera) {
			if(interfejs.nazwaTegoInterfejsu.equalsIgnoreCase(nazwaInterfejsu.substring(2))) {
				czyIstnieje=true;
				break;
			}
		}
		return czyIstnieje;
	}

}
