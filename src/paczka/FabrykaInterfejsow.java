package paczka;

/**
 * @author Andrzej
 *Klasa sluzaca do tworzenia odpowiednich interfejsow w zaleznosci od komendy uzytkownika 
 */
public class FabrykaInterfejsow {
	public InterfejsAbs interfejsDoZwrocenia=null;
	String maskaPodsieciInterfejsow="255.255.255.0";
	public Router router=null;
	
	public FabrykaInterfejsow(Router router) {
		this.router=router;
	}
	
	/**
	 * @param czescPierwszaPolecenia
	 * @param czescDrugaPolecenia
	 * @return
	 * Funkcja tworzaca nowe interfejsy
	 */
	public InterfejsAbs stworzInterfejs(String czescPierwszaPolecenia, String czescDrugaPolecenia) {
		
		if(czescPierwszaPolecenia.substring(0, 3).equalsIgnoreCase("ipA")) {
			interfejsDoZwrocenia=new InterfejsWejsciowy(czescPierwszaPolecenia, czescDrugaPolecenia, maskaPodsieciInterfejsow);
		}
		else if(czescPierwszaPolecenia.substring(0,3).equalsIgnoreCase("ipB")) {
			interfejsDoZwrocenia=new InterfejsWyjsciowy(czescPierwszaPolecenia, czescDrugaPolecenia, maskaPodsieciInterfejsow);
		}
		else if(czescPierwszaPolecenia.substring(0,3).equalsIgnoreCase("ipC")) {
			interfejsDoZwrocenia=new InterfejsKonsolowy(czescPierwszaPolecenia, czescDrugaPolecenia, maskaPodsieciInterfejsow);
		}
		router.zbiorInterfejsowRoutera.add(interfejsDoZwrocenia);
		
		return interfejsDoZwrocenia;
		
	}
	
	

}
