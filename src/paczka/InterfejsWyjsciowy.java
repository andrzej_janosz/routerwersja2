package paczka;

/**
 * @author Andrzej
 *Klasa reprezentujaca interfejs wyjsciowy
 */
public class InterfejsWyjsciowy extends InterfejsAbs {

	public static int licznikInterfejsowWyjsciowych=0;
	InterfejsWyjsciowy(String nazwaInterfejsu, String adresIpInterfejsu, String maskaPodsieci) {
		super(adresIpInterfejsu, maskaPodsieci);
		// TODO Auto-generated constructor stub
		rodzajTegoInterfejsu=RodzajInterfejsu.WYJSCIOWY;
		nazwaTegoInterfejsu=nazwaInterfejsu.substring(2);
		licznikInterfejsowWyjsciowych++;
	}
	
}
