package paczka;

/**
 * @author Andrzej
 *Klasa reprezentująca pojedynczy interfejs routera.
 */
public class Interfejs {
	
	public Adres adresInterfejsu;
	public RodzajInterfejsu rodzajInterfejsu;
	public String nazwaInterfejsu;
	
	/**
	 * @param adresIpInterfejsu
	 * @param maskaPodsieciInterfejsu
	 * @param rodzajInterfejsu
	 * @param nazwaInterfejsu
	 */
	public Interfejs(String adresIpInterfejsu, String maskaPodsieciInterfejsu, RodzajInterfejsu rodzajInterfejsu, String nazwaInterfejsu) {
		this.adresInterfejsu=new Adres(adresIpInterfejsu, maskaPodsieciInterfejsu);
		this.rodzajInterfejsu=rodzajInterfejsu;
		this.nazwaInterfejsu=nazwaInterfejsu;
	}
	
	/**Funkcja wyświetla na konsoli dane interfejsu
	 * 
	 */
	public void wyswietlDaneInterfejsu() {
		System.out.println("Interfejs " + this.nazwaInterfejsu + ": IP: " + this.adresInterfejsu.adresIp + " (maska:" + adresInterfejsu.maskaPodsieci + ")");
	}

}
