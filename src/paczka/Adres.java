package paczka;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Comparator;

/**
 * @author Andrzej
 *Klasa reprezentuj�ca adres Interfejsu: IP routera oraz jego mask�, kt�ra determinuje do jakiej podsieci nale�y dany Interfejs
 *dodane dla gita
 */
public class Adres  {
	
	public InetAddress adresIp;
	public InetAddress maskaPodsieci;
	
	/**
	 * @param adresIp
	 * @param maskaPodsieci
	 * Tworzenie nowego adresu o podanym IP i masc
	 */
	public Adres(String adresIp, String maskaPodsieci ) {
		try {
			this.adresIp=InetAddress.getByName(adresIp);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.err.println("Niepoprawny adres IP.");
		}
		try {
			this.maskaPodsieci=InetAddress.getByName(maskaPodsieci);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.err.println("Niepoprawny adres maski podsieci.");
		}
		
	}
	

	/**
	 * @param adresDoPorownania
	 * @return boolean
	 * Funkcja sprawdza czy dwa adresy naleza do tej samej podsieci
	 * Jesli liczba zgodnych oktetow dwoch adresow jest rowna dlugosci maski tych adresow zwracana jest wartosc true
	 */
	public boolean czyAdresWTejSamejPodsieci(Adres adresDoPorownania) {
		String[] doSprawdzenia=Adres.zwrocAdresGotowyDoParsowania(adresDoPorownania.adresIp);
		int dlugoscMaskiDoSprawdzenia=Adres.zwrocDlugoscMaski(adresDoPorownania);
		boolean wynikMetody=false;
		int licznikZgodnychOktetow=0;
		for(int j=0;j<dlugoscMaskiDoSprawdzenia; j++) {
			if(doSprawdzenia[j].equalsIgnoreCase(Adres.zwrocAdresGotowyDoParsowania(this.adresIp)[j])) {
				licznikZgodnychOktetow++;
			}
		}
		if(licznikZgodnychOktetow==dlugoscMaskiDoSprawdzenia) {
			wynikMetody=true;
		}
		
		return wynikMetody;
		
	}
	
	/**
	 * @param adres
	 * @return int 
	 * Funkcja zwraca dlugosc maski danego adresu(0-4)
	 */
	public static int zwrocDlugoscMaski(Adres adres) {
		String[] adresWTablicy=Adres.zwrocAdresGotowyDoParsowania(adres.maskaPodsieci);
		int licznikMaski=0;
		for(int j=0; j<adresWTablicy.length; j++) {
			if(!adresWTablicy[j].equalsIgnoreCase("0")) {
				licznikMaski++;
			}
		}
		return licznikMaski;
	}
	
	/**
	 * @param inetaddress
	 * @return
	 * Funkcja zwraca adres ip w postaci tablicy Stringow
	 */
	public static String[] zwrocAdresGotowyDoParsowania(InetAddress inetaddress) {
		String adres=inetaddress.toString();
		String adresBezSlasha=adres.substring(1);
		String[] adresWTablicy=adresBezSlasha.split("\\.");
		return adresWTablicy;
	}
	
	

}
