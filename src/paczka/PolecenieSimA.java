package paczka;

/**
 * @author Andrzej
 *Klasa odpowiedzialna za realizacje polecenia simA
 *
 */
public class PolecenieSimA extends Polecenie {

	public String wynikPolecenia=null;
	public Router router=null;
	public String ipDoSymulacji=null;
	public PolecenieSimA(Router router, String ipPrzekazane) {
		this.router=router;
		this.ipDoSymulacji=ipPrzekazane;
		
	}
	/* (non-Javadoc)
	 * @see paczka.Polecenie#wykonajPolecenie()
	 * Funkcja sprawdza czy interfejs o podanej nazwie istnieje i czy nalezy do tej samej podsieci co pakiet, jesli tak to przekazuje pakiet na dany interfejs
	 * W przypadku gdy pakiet skierowany jest na interfejs wejsciowy drukowane jest ostrzezenie o ataku
	 */
	@Override
	String wykonajPolecenie() {
		Adres adresDoSprawdzenia=new Adres(ipDoSymulacji, "255.255.255.0");
	
		for(InterfejsAbs interfejs : router.zbiorInterfejsowRoutera) {
			if(interfejs.adresTegoInterfejsu.czyAdresWTejSamejPodsieci(adresDoSprawdzenia)) {
				if( interfejs.nazwaTegoInterfejsu.startsWith("A")) {
					wynikPolecenia=("ATAK NA PORT A " + interfejs.adresTegoInterfejsu.adresIp.toString() + " PAKIET " + ipDoSymulacji);
				}
				else {
					wynikPolecenia=("Pakiet " + ipDoSymulacji + " przekazany na port " + interfejs.nazwaTegoInterfejsu + " (" + interfejs.adresTegoInterfejsu.adresIp.toString() + ")");
				}
			}
			else {
				wynikPolecenia=("Pakiet " + ipDoSymulacji + " ODRZUCONY");
			}
		}
		
		return wynikPolecenia;
	}

}
