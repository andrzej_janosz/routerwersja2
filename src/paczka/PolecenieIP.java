package paczka;

/**
 * @author Andrzej
 *Klasa odpowiedzialna za realizacje polecen IP
 */
public class PolecenieIP extends Polecenie {

	public InterfejsAbs interfejsAbs=null;
	public FabrykaInterfejsow fabrykaInterfejsow=null;
	String wynikPolecenia=null;
	public Router router=null;
	
	/**
	 * @param jeden
	 * @param dwa
	 * @param router
	 * Konstruktor odpowiedzialny za utworzenie interfejsu w oparciu o zadane przez uzytkownka dane
	 */
	public PolecenieIP(String jeden, String dwa, Router router) {
		this.router=router;
		fabrykaInterfejsow=new FabrykaInterfejsow(this.router);
		interfejsAbs=fabrykaInterfejsow.stworzInterfejs(jeden, dwa);
	}

	@Override
	String wykonajPolecenie() {
		// TODO Auto-generated method stub
		wynikPolecenia=("Adres portu " + interfejsAbs.nazwaTegoInterfejsu + " ustawiony na " + interfejsAbs.adresTegoInterfejsu.adresIp.toString()
				+ " (maska: " + interfejsAbs.adresTegoInterfejsu.maskaPodsieci.toString() + ")");
		return wynikPolecenia;
	}
	
}
