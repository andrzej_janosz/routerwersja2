package paczka;

import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * @author Andrzej
 *Klasa reprezentujaca model routera
 */
public class Router {
	
	public ArrayList<InterfejsAbs> zbiorInterfejsowRoutera=null;
	
	public Polecenie polecenie=null;
	
	public FabrykaPolecen fabrykaPolecen=null;
	
	public Router() {
		fabrykaPolecen=new FabrykaPolecen(this);
		zbiorInterfejsowRoutera=new ArrayList<InterfejsAbs>();
	}
	
	/**
	 * @param komenda Wartosc wprowadzona przez uzytkownika
	 * @return
	 * Funkcja pobiera wprowadzone polecenie i w oparciu o nie zwraca odpowiedni komunikat
	 */
	public String parsujPolecenie(String komenda) {
		
		ParserPolecen parserPolecen=new ParserPolecen();
		parserPolecen.podzielPolecenieUzytkownika(komenda);
		parserPolecen.przypiszPolecenia();
		
		String drugaCzescPolecenia=null;
		String czescPierwszaPolecenia=parserPolecen.zwrocPierwszaCzesc();
		if(parserPolecen.zwrocDrugaCzesc()!=null) {
			 drugaCzescPolecenia=parserPolecen.zwrocDrugaCzesc();
		}
		
		
		polecenie=fabrykaPolecen.stworzObiektPolecenia(czescPierwszaPolecenia, drugaCzescPolecenia);
		
		
		return polecenie.wykonajPolecenie();
		
	}
	
	
}
