package paczka;

/**
 * @author Andrzej
 *Klasa odpowiedzialna za realizacje polecenia Show
 */
public class PolecenieShow extends Polecenie {

	public String wynikPolecenia=null;
	public Router router=null;
	public PolecenieShow(Router router) {
		this.router=router;
		
	}
	@Override
	String wykonajPolecenie() {
		// TODO Auto-generated method stub
		for(InterfejsAbs interfejs : router.zbiorInterfejsowRoutera) {
			interfejs.wyswietlDaneInterfejsu();
		}
		
		return null;
	}
	
	private String[] wlasnaMetodaWykonujacaPolecenie() {
		
		return null;
		
	}

}
