package paczka;

/**
 * @author Andrzej
 *Klasa reprezentujaca abstrakcyjny interfejs, bez zdefiniowanego typu
 */
public abstract class InterfejsAbs {
	
	Adres adresTegoInterfejsu=null;
	RodzajInterfejsu rodzajTegoInterfejsu=null;
	String nazwaTegoInterfejsu=null;
	
	
	InterfejsAbs(String adresIpInterfejsu, String maskaPodsieci) {
		this.adresTegoInterfejsu=new Adres(adresIpInterfejsu, maskaPodsieci);
		
		
	}
	
	/**
	 * Funkcja wyswietlajaca dane inetrfejsu
	 */
	public void wyswietlDaneInterfejsu() {
		System.out.println("Interfejs " + this.nazwaTegoInterfejsu + ": IP: " + this.adresTegoInterfejsu.adresIp + " (maska:" + adresTegoInterfejsu.maskaPodsieci + ")");
	}

}
