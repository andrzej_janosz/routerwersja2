package paczka;

/**
 * @author Andrzej
 *Klasa odpowiedzialna za przeksztalcenie polecenia wpisanego przez uzytkownika
 */
public class ParserPolecen {
	
	public  String pierwszaCzescPolecenia=null;
	public  String drugaCzescPolecenia=null;
	public  String[] tablicaPolecen=null;
	
	/**
	 * @param wprowadzonePolecenie
	 * Funkcja dzieli wprowadzone polecenie na tablice Stringow
	 */
	public  void podzielPolecenieUzytkownika(String wprowadzonePolecenie) {
		tablicaPolecen=wprowadzonePolecenie.split(" ");
		
		
	}
	
	/**
	 * Funkcja przypisuje czesci polecen z wczesniej podzielonego polecenia wprowadzonego przez uzytkownika
	 */
	public  void przypiszPolecenia() {
		pierwszaCzescPolecenia=tablicaPolecen[0];
		if (tablicaPolecen.length>1) {
			drugaCzescPolecenia=tablicaPolecen[1];
		}
	}
	
	/**
	 * @return String-pierwsza czesc polecenia
	 */
	public  String zwrocPierwszaCzesc() {
		return pierwszaCzescPolecenia;
		
	}
	
	/**
	 * @return String-druga czesc polecenia
	 */
	public  String zwrocDrugaCzesc() {
		return drugaCzescPolecenia;
	}
	
	
	
	
	
	

}
